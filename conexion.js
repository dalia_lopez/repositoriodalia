console.log("Hoja");
// Inicialización de variables 
let graficas=document.getElementsByName("grafica");

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: "AIzaSyAap7SCpA7U4sjgmETFzR9sAPnNGnUOU7Q",
    authDomain: "proyectogeneral-9fae9.firebaseapp.com",
    projectId: "proyectogeneral-9fae9",
    storageBucket: "proyectogeneral-9fae9.appspot.com",
    messagingSenderId: "104663870142",
    appId: "1:104663870142:web:8a8ce45f9e610b9b470cef"
});

var db = firebase.firestore();

// Guardar
function guardar(){
    console.log("guardar");
    var nombre=document.getElementById("nombreVotante").value.trim();
    var votos=document.getElementById("selPartidos").value.trim();

    if(nombre.length==0){
        alert("Nombre vacio");
    }else{
        db.collection("voto").add({
            name: nombre,
            vot: votos
        })
        .then(function(docRef) {
            console.log("Document written with ID: ", docRef.id);
            document.getElementById("nombreVotante").value="";
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
        });
    }
}

// Lee datos
leer();
function leer(){
    db.collection("voto").onSnapshot((querySnapshot) => {
        var contador1=0;
        var contador2=0;
        var contador3=0;
        var contador4=0;
        var contador5=0;
        var total;
        querySnapshot.forEach((doc) => {
            var votos=doc.data().vot;

            if(votos=="morena"){
                contador1=contador1+1;
            }

            if(votos=="pri"){
                contador2=contador2+1;
            }

            if(votos=="prd"){
                contador3=contador3+1;
            }

            if(votos=="pan"){
                contador4=contador4+1;
            }

            if(votos=="pes"){
                contador5=contador5+1;
            }

            total=contador1+contador2+contador3+contador4+contador5;

            document.getElementById("votos1").value=contador1;
            document.getElementById("votos2").value=contador2;
            document.getElementById("votos3").value=contador3;
            document.getElementById("votos4").value=contador4;
            document.getElementById("votos5").value=contador5;
            document.getElementById("total").value=total;
        });
        dibjarGraficaDe(contador1,contador2,contador3,contador4,contador5);
    });
}


function dibjarGraficaDe(pa1, pa2, pa3, pa4, pa5){
    var canvas = document.getElementById('canvas').getContext('2d');
    var tipo;
    for (const g of graficas) {
        if(g.checked){
            tipo = g.value;
        }
    }
    new Chart(canvas, {
        type: tipo,
        data: {
            labels: ['MORENA', 'PRI', 'PRD', 'PAN', 'PES'],
            datasets: [{
                label: 'Gráfica',
                data: [pa1, pa2, pa3, pa4, pa5],
                backgroundColor: [
                    'rgb(179, 40, 45)',
                    'rgb(0, 104, 71)',
                    'rgb(255, 255, 0)',
                    'rgb(35, 85, 187)',
                    'rgb(82, 18, 252)'
                ],
                borderColor: [
                    'black',
                    'black',
                    'black',
                    'black',
                    'black'
                ],
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

// descargar pdf
function descargarPDF(){
    console.log("Si entra");
    try{
        // consultando base de datos voto
        db.collection("voto").get().then((query) => {
            let doc = new jsPDF();
            doc.setFontSize(8);// tamaño de la letra
            doc.text(90, 20,"Datos de la base\n");// encabezado
            // variables
            let text_num=0;
            let y=20;
            query.forEach((voto) => {
                text_num++;
                y+=10;// eje y
                // actual página
                doc.text(30, y, `
                    Nombre: ${voto.data().name}\tPartido: ${voto.data().vot}\t\n`);
                if(text_num==25){
                    // nueva página
                    doc.addPage();
                    voto="";
                    text_num=0;
                    y=20;
                }
            });

            var canvas = document.getElementById("canvas");
            doc.addImage(canvas.toDataURL(), 'PNG', 50, y+20, 100, 50);
            doc.save("datos.pdf");
        });
    }catch(error){
        alert("¡mensaje de error! ("+error+")");
    }
}